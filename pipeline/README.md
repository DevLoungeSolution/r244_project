# Logstash Pipelines

A Project for building and testing Logstash Pipelines. The project will attempt to check that pipelines are functioning
and provide a local development environment.

## Local Development

A local development environment is provided via the docker-compose file. Please run `rake dev` to start up the cluster.
You can shutdown the cluster by running `rake clean`.

##
Dependencies
Ruby installation
Docker installation

## 
Full dataset was used for analysis but the result was submitted with reduced data due to bandwidth restrictions.