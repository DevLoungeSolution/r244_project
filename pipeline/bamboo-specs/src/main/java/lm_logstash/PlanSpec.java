package lm_logstash;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.deployment.ReleaseNaming;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.builders.task.*;
import com.atlassian.bamboo.specs.builders.trigger.AfterSuccessfulBuildPlanTrigger;
import com.atlassian.bamboo.specs.builders.trigger.RemoteTrigger;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;
import com.atlassian.bamboo.specs.util.BambooServer;


@BambooSpec
public class PlanSpec {

    public static void main(final String[] args) throws Exception {
        //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("https://tools.standardbank.co.za/bamboo");
        Plan plan = new PlanSpec().createPlan();
        bambooServer.publish(plan);
    }

    Project project() {
        return new Project()
                .name("MELK Stack")
                .key("MS");
    }

    Plan createPlan() {
        return new Plan(project(), "lm_logstash", "LML")
                .description("Build and Test the SLAM logstash pipeline")
                .linkedRepositories("lm_logstash")
                .triggers(new RemoteTrigger())
                .planBranchManagement(new PlanBranchManagement()
                        .triggerBuildsLikeParentPlan()
                        .createForVcsBranch()
                        .delete(new BranchCleanup().whenRemovedFromRepository(true)))
                .stages(new Stage("Default Stage")
                        .jobs(new Job("Default Job", "JOB1")
                                .tasks(new VcsCheckoutTask()
                                                .description("Checkout Default Repository")
                                                .addCheckoutOfDefaultRepository()
                                                .cleanCheckout(true),
                                        new ScriptTask()
                                                .description("Bundle")
                                                .inlineBody("bundle install --path vendor/bundle"),
                                        new ScriptTask()
                                                .description("Build")
                                                .inlineBody("bundle exec rake build"),
                                        new ScriptTask()
                                                .description("Push")
                                                .inlineBody("if [[ ${bamboo.planRepository.branchName} == master ]]\n" +
                                                        "then\n" +
                                                        "bundle exec rake push\n" +
                                                        "fi")
                                )
                        )
                );
    }
}